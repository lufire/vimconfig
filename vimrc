" Lukas Feierabend custom .vimrc
"
" General

set history=700     " sets how many lines of history VIM has to remember
set autoread        " set to autoread when a file is changed from outside


" Color Scheme

colorscheme desert 
syntax enable		" enable syntax processing


" Spaces & Tabs

set tabstop=4		" number of visual spaces per tab
set softtabstop=4	" number of spaces in tab when editing
set expandtab		" tabs are spaces
set shiftwidth=4


" UI Config

set number		" show line numbers
set showcmd		" show command in bottom bar
set cursorline		" highlight current line
set so=7            " # of lines after/before cursor when scrolling
set ruler           " always show current position
set cmdheight=2     " height of command bar
set hid             " a buffer becomes hidden when abandoned
set backspace=eol,start,indent  " configure backspace

set whichwrap+=<,>,h,l          " "
set splitbelow  " opens new horizontal split screen below
set splitright  " opens new vertical split screen right

filetype indent on	" load filetype-specific indent files

set wildmenu		" visual autocomplete for command menu

set lazyredraw		" redraw only when we need to

set showmatch		" highlight matching [{()}]


" Searching

set incsearch		" search as characters are entered
set hlsearch		" highlight matches

" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>


" Folding

set foldenable		" enable folding
set foldlevelstart=10	" open most folds by default
set foldnestmax=10	" 10 nested fold max

" space open/closes folds
" nnoremap <space> za

set foldmethod=indent	" fold based on indent level


" Movement

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" move to beginning/end of line
nnoremap B ^
nnoremap E $


" map hjkl to jklö but with down-up-left-right configuration
noremap ö l
noremap l k
noremap k j
noremap j h

" move between windows
nnoremap <C-ö> <C-W><C-ö>
nnoremap <C-l> <C-W><C-l>
nnoremap <C-k> <C-W><C-k>
nnoremap <C-j> <C-W><C-j>

" highlight last inserted text
nnoremap gV `[v`]

" Put newline in normal mode
nnoremap <S-Enter> O<Esc>
nnoremap <CR> o<Esc>


" Leader Shortcuts

let mapleader=","	" leader is comma

" jk is escape
inoremap hh <Esc>

" toggle gundo
nnoremap <leader>u :GundoToggle<CR>

" edit vimrc/zshrc and load vimrc bindings
nnoremap <leader>ev :vsp $MYVIMRC<CR>
nnoremap <leader>ez :vsp ~/.zshrc<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>

" save session
nnoremap <leader>s :mksession<CR>

" open ag.vim
nnoremap <leader>a :Ag


" CtrIP

" CtrIP settings
let g:ctrlp_match_window = 'bottom,order::ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

" enable all Python syntax highlighting
let python_highlight_all = 1

" Activate mouse
set mouse=a